window.onload = function () {
    if(document.URL.indexOf("code") > -1) {
        let url = new URL(document.URL)
        const code = url.searchParams.get('code');
        sendCode(code, "google")
    } else {
        console.log("oops")
    }
}


function sendCode(code, service){
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:8080/auth/auth-via/' + service, false);
    xhr.setRequestHeader('Content-Type', 'application/json')
    let data = {
        "code": code
    };
    xhr.send(JSON.stringify(data))

    if(xhr.status == 200){
        let response = JSON.parse(xhr.responseText)
        console.log(response)
        localStorage.setItem("username", response.userData.email)
        localStorage.setItem("access_token", response.token.accessToken)
        localStorage.setItem("refresh_token", response.token.refreshToken)
        setTimeout(() => console.log("Did you see this perfect loading?"), 3000);
        window.close();
    } else {
        document.getElementById("info").innerText = xhr.responseText;

    }
}

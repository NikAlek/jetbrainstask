

window.onload = function () {
    let username = localStorage.getItem("username")
    if(username != null){
        document.getElementById("greetings").innerText = "Welcome, " + username +  "!This is a home page"
    } else {
        document.getElementById("greetings").innerText = "Welcome, new user! This is a home page"
    }
    document.getElementById("github").onclick = () => window.open("https://github.com/JDNik")
    document.getElementById("vk").onclick = () => window.open("https://vk.com/sneakythrows")
}

function signOut(){
    localStorage.clear();
    location.reload();
}
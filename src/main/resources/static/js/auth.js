let GOOGLE_CODE_URI = "https://accounts.google.com/o/oauth2/v2/auth";
let GOOGLE_CLIENT_ID = "952039803867-qet243qs283183ea5tfub0d4m1ri79ma.apps.googleusercontent.com";
let REDIRECT_URI = "http://localhost:8080"
let RESPONSE_TYPE = "code";
let SCOPES = "email profile";



window.onload = function () {
    let username = localStorage.getItem("username")
    if (username != null) {
        document.getElementById("title").innerText = "You are already signed in as " + username;
        document.getElementById("googleButton").setAttribute('disabled', 'disabled')
        document.getElementById("googleButton").style.borderColor = "#ff0000"
    } else {
        document.getElementById("title").innerText = "You can sign up with your google account"
    }

}

function processAuth() {
    let popup = window.open(access_uri(), "auth-via", "width=900,height=900,resizable=no")
    const interval = window.setInterval( function () {
            if (popup.closed) {
                window.clearInterval(interval);
                document.getElementById("toHome").click();

            }
        }, 100
    )
}



function access_uri() {
    return GOOGLE_CODE_URI + "?"
        + "client_id=" + GOOGLE_CLIENT_ID + "&"
        + "redirect_uri=" + REDIRECT_URI + "&"
        + "response_type=" + RESPONSE_TYPE + "&"
        + "scope=" + SCOPES;
}
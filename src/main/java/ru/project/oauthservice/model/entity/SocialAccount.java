package ru.project.oauthservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.model.entity.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;


/**
 * External service account data
 * In real app user may have many account from different services
 * this entity contains user data from social accounts
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "users_social")
public class SocialAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private OAuthType oAuthType;

    private String email;
    private String firstName;
    private String lastName;
    private LocalDateTime created;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @PrePersist
    private void onCreate() {
        this.setCreated(LocalDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialAccount that = (SocialAccount) o;
        return oAuthType == that.oAuthType && email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oAuthType, email);
    }
}

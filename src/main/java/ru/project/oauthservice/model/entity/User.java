package ru.project.oauthservice.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


/**
 * user entity
 * contains user information and social account set
 * (in this task user can contains only google account)
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long userId;

  @Column(unique = true)
  private String email;

  private String firstName;
  private String lastName;
  private LocalDateTime created;
  private LocalDateTime modified;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  private Set<SocialAccount> externalAccounts = new HashSet<>();

  @PrePersist
  private void onCreate() {
    this.setCreated(LocalDateTime.now());
    this.setModified(LocalDateTime.now());
  }

  @PreUpdate
  private void onUpdate() {
    this.setModified(LocalDateTime.now());
  }
}

package ru.project.oauthservice.model.entity;

/**
 * external service type
 * ( in this task only google, but to easy add new services)
 */
public enum OAuthType {
    GOOGLE,
    OTHER;
}

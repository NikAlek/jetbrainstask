package ru.project.oauthservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * dto with token pair
 * contains access and refresh tokens
 */
@AllArgsConstructor
@Data
public class TokenDTO {
    private String accessToken;
    private String refreshToken;
}

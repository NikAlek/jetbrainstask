package ru.project.oauthservice.model.dto;

import lombok.Data;


/**
 * dto from client
 * contains code to get access token for external service
 */
@Data
public class CodeDTO {
    String code;
}

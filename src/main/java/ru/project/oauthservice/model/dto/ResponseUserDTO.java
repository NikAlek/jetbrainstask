package ru.project.oauthservice.model.dto;

import lombok.Builder;
import lombok.Data;

/**
 * dto with user data (in this task only email)
 */
@Builder
@Data
public class ResponseUserDTO {
    String email;
}

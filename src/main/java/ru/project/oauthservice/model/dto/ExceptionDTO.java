package ru.project.oauthservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;

/**
 * dto for exceptions
 * contains data for debugging
 */
@Data
@AllArgsConstructor
public class ExceptionDTO {
    private String message;
    private LocalDate throwTime;
    private HttpStatus httpStatus;
}

package ru.project.oauthservice.model.dto;

import lombok.Data;

/**
 * dto from client for registration in app
 */
@Data
public class UserDTO {
    private String email;
    private String password;
}

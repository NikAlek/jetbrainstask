package ru.project.oauthservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * dto for success auth response
 * contains data for client (token pair and user data)
 */
@AllArgsConstructor
@Data
public class AuthResponseDTO {
  private TokenDTO token;
  private ResponseUserDTO userData;
}

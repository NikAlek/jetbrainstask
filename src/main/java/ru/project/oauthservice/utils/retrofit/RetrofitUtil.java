package ru.project.oauthservice.utils.retrofit;

import lombok.extern.log4j.Log4j2;
import retrofit2.Call;
import retrofit2.Response;
import ru.project.oauthservice.utils.retrofit.exceptions.RetrofitExecutionException;

@Log4j2
public class RetrofitUtil {
  public static <T> T executeCall(Call<T> call) throws RetrofitExecutionException {
    try {
      Response<T> response = call.execute();
      if (!response.isSuccessful() || response.body() == null) {
        return null;
      }
      return response.body();
    } catch (Exception e) {
      log.warn("Error while processing retrofit execution");
      throw new RetrofitExecutionException(e.getLocalizedMessage());
    }
  }
}

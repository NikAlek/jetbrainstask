package ru.project.oauthservice.utils.retrofit.exceptions;

public class RetrofitExecutionException extends Exception {

  public RetrofitExecutionException() {}

  public RetrofitExecutionException(String message) {
    super(message);
  }
}

package ru.project.oauthservice.utils.token;

public enum TokenType {
    REFRESH,
    ACCESS;
}

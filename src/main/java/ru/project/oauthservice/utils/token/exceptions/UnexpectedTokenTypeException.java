package ru.project.oauthservice.utils.token.exceptions;

public class UnexpectedTokenTypeException extends RuntimeException {
  public UnexpectedTokenTypeException() {}

  public UnexpectedTokenTypeException(String message) {
    super(message);
  }
}

package ru.project.oauthservice.utils.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.utils.token.exceptions.UnexpectedTokenTypeException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtUtil {
  @Value("${jwt.secret-word}")
  private String SECRET_WORD;

  @Value("${jwt.access-token-life-time}")
  private int accessTokenLifeTime;

  @Value("${jwt.refresh-token-life-time}")
  private int refreshTokenLifeTime;

  public String extractUserEmailFromToken(String token) {
    return extractClaims(token, Claims::getSubject);
  }

  public Date extractExpirationFromToken(String token) {
    return extractClaims(token, Claims::getExpiration);
  }

  public <T> T extractClaims(String token, Function<Claims, T> claimsTFunction) {
    final Claims claims = extractAllClaims(token);
    return claimsTFunction.apply(claims);
  }

  private Claims extractAllClaims(String token) {
    return Jwts.parser().setSigningKey(SECRET_WORD).parseClaimsJws(token).getBody();
  }

  private Boolean isTokenExpired(String token) {
    return extractExpirationFromToken(token).before(new Date());
  }

  public String generateToken(User user, TokenType tokenType) {
    Map<String, Object> claims = new HashMap<>();
    switch (tokenType) {
      case ACCESS:
        return createToken(claims, user, accessTokenLifeTime);
      case REFRESH:
        return createToken(claims, user, refreshTokenLifeTime);
      default:
        throw new UnexpectedTokenTypeException();
    }
  }

  private String createToken(Map<String, Object> claims, User user, int tokenLifeTime) {
    return Jwts.builder()
            .setClaims(claims)
            .setSubject(user.getEmail())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + tokenLifeTime))
            .signWith(SignatureAlgorithm.HS512, SECRET_WORD)
            .compact();
  }

  public Boolean validateToken(String token, User user) {
    final String userEmail = extractUserEmailFromToken(token);
    return (userEmail.equals(user.getEmail()) && !isTokenExpired(token));
  }
}

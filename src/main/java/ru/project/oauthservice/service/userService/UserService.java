package ru.project.oauthservice.service.userService;

import ru.project.oauthservice.model.dto.UserDTO;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.model.entity.SocialAccount;

/**
 * service for user logic
 */
public interface UserService {

  User createUser(UserDTO userDTO);

  User addNewSocialAccount(SocialAccount socialAccount, String email);
}

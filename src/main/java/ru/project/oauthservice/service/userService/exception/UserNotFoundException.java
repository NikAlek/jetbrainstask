package ru.project.oauthservice.service.userService.exception;

public class UserNotFoundException extends Exception {

  public UserNotFoundException() {}

  public UserNotFoundException(String message) {
    super(message);
  }
}

package ru.project.oauthservice.service.userService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ru.project.oauthservice.model.repositories.SocialAccountRepository;
import ru.project.oauthservice.model.repositories.UserRepository;
import ru.project.oauthservice.model.dto.UserDTO;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.model.entity.SocialAccount;
import ru.project.oauthservice.service.factories.userFactory.UserFactory;
import ru.project.oauthservice.service.userService.UserService;

import java.util.Optional;

@Log4j2
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final SocialAccountRepository socialAccountRepository;
    private final UserFactory userFactory;

    @Override
    @Deprecated
    public User createUser(UserDTO userDTO) {
        return null;
    }

    @Override
    public User addNewSocialAccount(SocialAccount socialAccount, String email) {
        User user =
                userRepository
                        .findByEmail(email)
                        .orElseGet(() -> userFactory.createUserFromSocialAccount(socialAccount));
        log.info("Add new social account to user {}", () -> email);

        if (user.getExternalAccounts().contains(socialAccount)) {
            return user;
        }
        userRepository.saveAndFlush(user);
        socialAccount.setUser(user);
        socialAccountRepository.saveAndFlush(socialAccount);
        return user;
    }
}

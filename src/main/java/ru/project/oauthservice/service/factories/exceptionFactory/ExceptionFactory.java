package ru.project.oauthservice.service.factories.exceptionFactory;

import org.springframework.http.HttpStatus;
import ru.project.oauthservice.model.dto.ExceptionDTO;

/**
 * factory for exceptions dto
 */
public interface ExceptionFactory {
    ExceptionDTO exceptionDTO(String message, HttpStatus httpStatus);
}

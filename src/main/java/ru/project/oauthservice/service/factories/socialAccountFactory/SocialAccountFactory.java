package ru.project.oauthservice.service.factories.socialAccountFactory;

import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.model.entity.SocialAccount;

/**
 * factory for social account entity
 */
public interface SocialAccountFactory {
    SocialAccount createUserSocial(String name, String surname, String email, OAuthType oAuthType);
}

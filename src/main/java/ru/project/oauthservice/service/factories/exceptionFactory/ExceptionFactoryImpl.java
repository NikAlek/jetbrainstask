package ru.project.oauthservice.service.factories.exceptionFactory;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.project.oauthservice.model.dto.ExceptionDTO;

import java.time.LocalDate;

@Service
public class ExceptionFactoryImpl implements ExceptionFactory{
    @Override
    public ExceptionDTO exceptionDTO(String message, HttpStatus httpStatus) {
        return new ExceptionDTO(message, LocalDate.now(), httpStatus);
    }
}

package ru.project.oauthservice.service.factories.socialAccountFactory;

import org.springframework.stereotype.Component;
import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.model.entity.SocialAccount;

@Component
public class SocialAccountFactoryImpl implements SocialAccountFactory{
    @Override
    public SocialAccount createUserSocial(String name, String surname, String email, OAuthType oAuthType) {
        return SocialAccount.builder()
                .email(email)
                .oAuthType(oAuthType)
                .firstName(name)
                .lastName(surname)
                .build();
    }
}

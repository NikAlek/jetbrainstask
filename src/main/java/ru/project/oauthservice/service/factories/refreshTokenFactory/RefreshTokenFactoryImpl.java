package ru.project.oauthservice.service.factories.refreshTokenFactory;

import org.springframework.stereotype.Component;
import ru.project.oauthservice.model.entity.RefreshToken;
import ru.project.oauthservice.model.entity.User;

import java.util.Date;

@Component
public class RefreshTokenFactoryImpl implements RefreshTokenFactory{
    @Override
    public RefreshToken createRefreshToken(String token, Date expire, User user) {
        return RefreshToken.builder()
                .expireDate(expire)
                .token(token)
                .user(user)
                .build();
    }
}

package ru.project.oauthservice.service.factories.refreshTokenFactory;

import ru.project.oauthservice.model.entity.RefreshToken;
import ru.project.oauthservice.model.entity.User;

import java.util.Date;

/**
 * factory for refresh token entity
 */
public interface RefreshTokenFactory {
    RefreshToken createRefreshToken(String token, Date expire, User user);
}

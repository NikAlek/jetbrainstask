package ru.project.oauthservice.service.factories.userFactory;

import ru.project.oauthservice.model.dto.UserDTO;
import ru.project.oauthservice.model.entity.SocialAccount;
import ru.project.oauthservice.model.entity.User;

/**
 * factory for user entity
 */

public interface UserFactory {
    User createDefaultUser(UserDTO userDTO);
    User createUserFromSocialAccount(SocialAccount socialAccount);
}

package ru.project.oauthservice.service.factories.userFactory;

import org.springframework.stereotype.Component;
import ru.project.oauthservice.model.dto.UserDTO;
import ru.project.oauthservice.model.entity.SocialAccount;
import ru.project.oauthservice.model.entity.User;

import java.util.HashSet;

@Component
public class UserFactoryImpl implements UserFactory{
    @Override
    public User createDefaultUser(UserDTO userDTO) {
        return null;
    }

    @Override
    public User createUserFromSocialAccount(SocialAccount socialAccount) {
        return User.builder()
                .email(socialAccount.getEmail())
                .firstName(socialAccount.getFirstName())
                .lastName(socialAccount.getLastName())
                .email(socialAccount.getEmail())
                .externalAccounts(new HashSet<>())
                .build();
    }
}

package ru.project.oauthservice.service.tokenService.exceptions;

public class InvalidRefreshTokenException extends Exception{
    public InvalidRefreshTokenException() {
    }

    public InvalidRefreshTokenException(String message) {
        super(message);
    }
}

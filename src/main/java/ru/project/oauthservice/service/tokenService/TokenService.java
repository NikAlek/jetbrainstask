package ru.project.oauthservice.service.tokenService;

import ru.project.oauthservice.model.dto.TokenDTO;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.service.tokenService.exceptions.InvalidRefreshTokenException;
import ru.project.oauthservice.service.tokenService.exceptions.UnknownRefreshTokenException;

/**
 * service for token logic
 */
public interface TokenService {
  TokenDTO generateToken(User user);

  TokenDTO refreshToken(String refreshToken) throws UnknownRefreshTokenException, InvalidRefreshTokenException;
}

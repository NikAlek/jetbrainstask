package ru.project.oauthservice.service.tokenService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ru.project.oauthservice.model.dto.TokenDTO;
import ru.project.oauthservice.model.entity.RefreshToken;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.model.repositories.RefreshTokenRepository;
import ru.project.oauthservice.model.repositories.UserRepository;
import ru.project.oauthservice.service.factories.refreshTokenFactory.RefreshTokenFactory;
import ru.project.oauthservice.service.tokenService.exceptions.InvalidRefreshTokenException;
import ru.project.oauthservice.service.tokenService.exceptions.UnknownRefreshTokenException;
import ru.project.oauthservice.utils.token.JwtUtil;
import ru.project.oauthservice.utils.token.TokenType;

@Log4j2
@RequiredArgsConstructor
@Service
public class TokenServiceImpl implements TokenService {

    private final JwtUtil jwtUtil;
    private final UserRepository userRepository;
    private final RefreshTokenRepository tokenRepository;
    private final RefreshTokenFactory tokenFactory;

    @Override
    public TokenDTO generateToken(User user) {
        log.info("start generate token for user {}", user::getEmail);
        final String accessToken = jwtUtil.generateToken(user, TokenType.ACCESS);
        final String refreshToken = jwtUtil.generateToken(user, TokenType.REFRESH);
        RefreshToken token =
                tokenFactory.createRefreshToken(
                        refreshToken, jwtUtil.extractExpirationFromToken(refreshToken), user);
        tokenRepository.saveAndFlush(token);
        log.info("New token pair successfully created");
        return new TokenDTO(accessToken, refreshToken);
    }

    @Override
    public TokenDTO refreshToken(String refreshToken)
            throws UnknownRefreshTokenException, InvalidRefreshTokenException {
        RefreshToken token =
                tokenRepository.findByToken(refreshToken).orElseThrow(UnknownRefreshTokenException::new);
        User user = token.getUser();
        if (!jwtUtil.validateToken(token.getToken(), user)) {
            throw new InvalidRefreshTokenException();
        }
        log.info("start update token for user {}", user::getEmail);
        tokenRepository.delete(token);
        return generateToken(user);
    }
}

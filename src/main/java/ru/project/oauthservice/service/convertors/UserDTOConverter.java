package ru.project.oauthservice.service.convertors;

import org.springframework.stereotype.Component;
import ru.project.oauthservice.model.dto.ResponseUserDTO;
import ru.project.oauthservice.model.entity.User;

@Component
public class UserDTOConverter implements Converter<User, ResponseUserDTO> {
    @Override
    public ResponseUserDTO toDTO(User user) {
        return ResponseUserDTO.builder()
                .email(user.getEmail())
                .build();
    }
}

package ru.project.oauthservice.service.convertors;



import org.apache.commons.lang3.NotImplementedException;

/**
 * simple converter from entity to dto
 * @param <Entity>
 * @param <DTO>
 */
public interface Converter<Entity, DTO> {
    default DTO toDTO(Entity entity) {
        throw new NotImplementedException();
    }

    default Entity toEntity(DTO dto) {
        throw new NotImplementedException();
    }
}
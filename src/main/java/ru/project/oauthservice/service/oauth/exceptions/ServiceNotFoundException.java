package ru.project.oauthservice.service.oauth.exceptions;

public class ServiceNotFoundException extends Exception {

    public ServiceNotFoundException() {
    }

    public ServiceNotFoundException(String message) {
        super(message);
    }
}

package ru.project.oauthservice.service.oauth;

import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.service.oauth.exceptions.ServiceNotFoundException;
import ru.project.oauthservice.service.oauth.service.exceptions.AccessTokenRequestException;
import ru.project.oauthservice.service.oauth.service.exceptions.UserRequestException;

/**
 * oauth service
 * make request for external service to ger user data
 * @param <Entity> produced entity
 */
public interface OAuthService <Entity> {
    /**
     * method for get user data
     *
     * @param oAuthType external service name
     * @param code code for get access token
     *
     * @return user
     * @throws ServiceNotFoundException - unknown AuthType
     * @throws UserRequestException - exception while send request for user data
     * @throws AccessTokenRequestException - exception while exchange code for token
     */
    Entity processOAuthUser(OAuthType oAuthType, String code) throws ServiceNotFoundException, UserRequestException, AccessTokenRequestException;
}

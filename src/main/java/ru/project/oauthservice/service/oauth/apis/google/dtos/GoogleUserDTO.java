package ru.project.oauthservice.service.oauth.apis.google.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoogleUserDTO {
  private String givenName;
  private String familyName;
  private String email;
}

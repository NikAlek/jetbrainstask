package ru.project.oauthservice.service.oauth.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.model.entity.SocialAccount;
import ru.project.oauthservice.service.factories.socialAccountFactory.SocialAccountFactory;
import ru.project.oauthservice.service.oauth.apis.google.GoogleOAuthApi;
import ru.project.oauthservice.service.oauth.apis.google.dtos.GoogleCodeDTO;
import ru.project.oauthservice.service.oauth.apis.google.dtos.GoogleTokenDTO;
import ru.project.oauthservice.service.oauth.apis.google.dtos.GoogleUserDTO;
import ru.project.oauthservice.service.oauth.service.OAuthExecuteService;
import ru.project.oauthservice.service.oauth.service.exceptions.AccessTokenRequestException;
import ru.project.oauthservice.service.oauth.service.exceptions.UserRequestException;
import ru.project.oauthservice.utils.retrofit.RetrofitUtil;
import ru.project.oauthservice.utils.retrofit.exceptions.RetrofitExecutionException;

import java.util.Objects;


@Log4j2
@RequiredArgsConstructor
@Component
public class GoogleOAuthService implements OAuthExecuteService {
  private final SocialAccountFactory socialFactory;
  private final GoogleOAuthApi googleOAuthApi;

  @Value("${google.client-id}")
  private String clientId;

  @Value("${google.client-secret}")
  private String clientSecret;

  @Value("${google.redirect-uri}")
  private String redirectUri;

  @Value("${google.grant-type}")
  private String grantType;

  @Override
  public String getToken(String code) throws AccessTokenRequestException {
    try {
      GoogleCodeDTO googleCodeDTO =
          new GoogleCodeDTO(clientId, clientSecret, code, redirectUri, grantType);
      log.info("google code dto = " + googleCodeDTO);
      GoogleTokenDTO googleTokenDTO =
          RetrofitUtil.executeCall(googleOAuthApi.getToken(googleCodeDTO));
      if (Objects.isNull(googleTokenDTO)) {
        throw new AccessTokenRequestException();
      }
      return googleTokenDTO.getAccessToken();
    } catch (RetrofitExecutionException e) {
      log.warn("got retrofit execution exception!. Caused by {}", e::getCause);
      throw new AccessTokenRequestException(e.getLocalizedMessage());
    }
  }

  @Override
  public SocialAccount getUser(String token) throws UserRequestException {
    try {
      GoogleUserDTO googleUserDTO = RetrofitUtil.executeCall(googleOAuthApi.getUser(token));
      if (Objects.isNull(googleUserDTO)) {
        throw new UserRequestException();
      }
      log.info("User with email {} successfully upload from google!", googleUserDTO::getEmail);
      return socialFactory.createUserSocial(
          googleUserDTO.getGivenName(),
          googleUserDTO.getFamilyName(),
          googleUserDTO.getEmail(),
          OAuthType.GOOGLE);
    } catch (RetrofitExecutionException e) {
      log.warn("got retrofit execution exception!. Caused by {}", e::getCause);
      throw new UserRequestException(e.getLocalizedMessage());
    }
  }
}

package ru.project.oauthservice.service.oauth.apis.google;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.project.oauthservice.service.oauth.apis.google.dtos.GoogleCodeDTO;
import ru.project.oauthservice.service.oauth.apis.google.dtos.GoogleTokenDTO;
import ru.project.oauthservice.service.oauth.apis.google.dtos.GoogleUserDTO;

public interface GoogleOAuthApi {
  @POST("https://oauth2.googleapis.com/token")
  Call<GoogleTokenDTO> getToken(@Body GoogleCodeDTO googleCodeDTO);

  @GET("https://www.googleapis.com/oauth2/v1/userinfo")
  Call<GoogleUserDTO> getUser(@Query("access_token") String accessToken);
}

package ru.project.oauthservice.service.oauth.service;

import ru.project.oauthservice.model.entity.SocialAccount;
import ru.project.oauthservice.service.oauth.service.exceptions.AccessTokenRequestException;
import ru.project.oauthservice.service.oauth.service.exceptions.UserRequestException;

/**
 * execute service for external services
 */
public interface OAuthExecuteService {
  String getToken(String code) throws AccessTokenRequestException;
  SocialAccount getUser(String token) throws UserRequestException;
}

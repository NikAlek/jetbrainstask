package ru.project.oauthservice.service.oauth.service.exceptions;

public class UserRequestException extends Exception {
  public UserRequestException() {}

  public UserRequestException(String message) {
    super(message);
  }
}

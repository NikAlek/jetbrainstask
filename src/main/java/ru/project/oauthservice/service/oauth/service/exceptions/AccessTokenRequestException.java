package ru.project.oauthservice.service.oauth.service.exceptions;

public class AccessTokenRequestException extends Exception {
  public AccessTokenRequestException() {}

  public AccessTokenRequestException(String message) {
    super(message);
  }
}

package ru.project.oauthservice.service.oauth;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.model.entity.SocialAccount;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.service.oauth.exceptions.ServiceNotFoundException;
import ru.project.oauthservice.service.oauth.service.OAuthExecuteService;
import ru.project.oauthservice.service.oauth.service.exceptions.AccessTokenRequestException;
import ru.project.oauthservice.service.oauth.service.exceptions.UserRequestException;
import ru.project.oauthservice.service.oauth.service.impls.GoogleOAuthService;
import ru.project.oauthservice.service.userService.UserService;

import java.util.Map;
import java.util.Objects;

@Log4j2
@Service
public class OAuthServiceImpl implements OAuthService<User>{
  private final UserService userService;
  private final Map<OAuthType, ? extends OAuthExecuteService> serviceToApi;

  public OAuthServiceImpl(UserService userService, GoogleOAuthService googleOAuthService) {
    this.userService = userService;
    serviceToApi = Map.of(OAuthType.GOOGLE, googleOAuthService);
    log.info("OAuth service init!");
  }

  public User processOAuthUser(OAuthType oAuthType, String code)
      throws ServiceNotFoundException, UserRequestException, AccessTokenRequestException {
    final OAuthExecuteService service = serviceToApi.get(oAuthType);
    if (Objects.isNull(service)) {
      log.warn("Service {} not found!", () -> oAuthType);
      throw new ServiceNotFoundException();
    }
    final String token = service.getToken(code);
    final SocialAccount user = service.getUser(token);
    return userService.addNewSocialAccount(user, user.getEmail());
  }
}

package ru.project.oauthservice.service.oauth.apis.google.dtos;

import lombok.Data;

@Data
public class GoogleCodeDTO {
  private final String client_id;
  private final String client_secret;
  private final String code;
  private final String redirect_uri;
  private final String grant_type;
}

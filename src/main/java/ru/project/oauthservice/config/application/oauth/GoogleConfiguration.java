package ru.project.oauthservice.config.application.oauth;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * Custom configuration for google api
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "google")
public class GoogleConfiguration {
    private String clientId;
    private String clientSecret;
    private String redirectUri;
    private String grantType;
}

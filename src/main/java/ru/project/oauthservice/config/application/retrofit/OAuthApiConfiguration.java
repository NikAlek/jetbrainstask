package ru.project.oauthservice.config.application.retrofit;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import ru.project.oauthservice.service.oauth.apis.google.GoogleOAuthApi;

@RequiredArgsConstructor
@Configuration
public class OAuthApiConfiguration {
    private final Retrofit retrofit;

    @Bean
    GoogleOAuthApi getGoogleOAuthApi() {
        return retrofit.create(GoogleOAuthApi.class);
    }
}

package ru.project.oauthservice.config.application.token;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * custom configuration for jwt
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "jwt")
public class JwtConfiguration {
  private String  secretWord;
  private Integer accessTokenLifeTime;
  private Integer refreshTokenLifeTime;
}

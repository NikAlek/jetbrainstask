package ru.project.oauthservice.config.application.retrofit;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;


/**
 * retrofit configuration
 *
 * object mapper configured for snake case for success convert api response to dto
 * okhttpclient have standart configuration
 */
@Configuration
public class RetrofitConfiguration {
    @Bean
    public Retrofit getRetrofit() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

        OkHttpClient okHttpClient =
                new OkHttpClient.Builder()
                        .addInterceptor(httpLoggingInterceptor)
                        .readTimeout(120, TimeUnit.SECONDS)
                        .build();

        ObjectMapper objectMapper =
                new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS, true)
                        .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        return new Retrofit.Builder()
                .baseUrl("https://localhost")
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .build();
    }
}
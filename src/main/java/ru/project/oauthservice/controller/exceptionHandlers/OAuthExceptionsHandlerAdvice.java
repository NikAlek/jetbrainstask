package ru.project.oauthservice.controller.exceptionHandlers;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.project.oauthservice.model.dto.ExceptionDTO;
import ru.project.oauthservice.service.factories.exceptionFactory.ExceptionFactory;
import ru.project.oauthservice.service.oauth.exceptions.ServiceNotFoundException;
import ru.project.oauthservice.service.oauth.service.exceptions.AccessTokenRequestException;
import ru.project.oauthservice.service.oauth.service.exceptions.UserRequestException;

/**
 * handler for retrofit processing
 */
@Log4j2
@RequiredArgsConstructor
@ControllerAdvice
public class OAuthExceptionsHandlerAdvice {

    private final ExceptionFactory exceptionFactory;

    private final String RETROFIT_EXCEPTION_MESSAGE = "Exception while processing retrofit requests";
    private final String SERVICE_NOT_FOUND_MESSAGE = "This service does not exist!";

    /**
     * special method for generating response if retrofit throws ATRE or URE
     *
     * @return response entity with status code 523 and exception data
     */
    @ExceptionHandler(value = {AccessTokenRequestException.class, UserRequestException.class})
    public ResponseEntity<ExceptionDTO> produceRetrofitException(){
       ExceptionDTO exceptionDTO = exceptionFactory.exceptionDTO(
               RETROFIT_EXCEPTION_MESSAGE,
               HttpStatus.resolve(523));
       return ResponseEntity.status(523).body(exceptionDTO);
    }

    /**
     * special method for generating response if retrofit throws SNFE
     *
     * @return response entity with status code 406 and exception data
     */
    @ExceptionHandler(value = {ServiceNotFoundException.class})
    public ResponseEntity<ExceptionDTO> produceOAuthServiceException(){
        ExceptionDTO exceptionDTO = exceptionFactory.exceptionDTO(
                SERVICE_NOT_FOUND_MESSAGE,
                HttpStatus.NOT_ACCEPTABLE
        );
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(exceptionDTO);
    }


}

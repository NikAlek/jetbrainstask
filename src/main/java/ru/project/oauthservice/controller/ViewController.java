package ru.project.oauthservice.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * controller to send html
 */
@Controller
@RequestMapping("/")
public class ViewController {

    @GetMapping("/home")
    public String getHomePage(){
        return "index";
    }

    @GetMapping("/signin")
    public String getAuthPage(){
        return "auth";
    }

    @GetMapping("/")
    public String getTempPage(){
        return "temp";
    }

}

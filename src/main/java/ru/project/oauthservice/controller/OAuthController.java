package ru.project.oauthservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.project.oauthservice.model.dto.AuthResponseDTO;
import ru.project.oauthservice.model.dto.CodeDTO;
import ru.project.oauthservice.model.dto.ResponseUserDTO;
import ru.project.oauthservice.model.dto.TokenDTO;
import ru.project.oauthservice.model.entity.OAuthType;
import ru.project.oauthservice.model.entity.User;
import ru.project.oauthservice.service.convertors.Converter;
import ru.project.oauthservice.service.oauth.OAuthService;
import ru.project.oauthservice.service.tokenService.TokenService;


/**
 * controller for oauth requests
 */
@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class OAuthController {

  private final TokenService tokenService;
  private final OAuthService<User> oAuthService;
  private final Converter<User, ResponseUserDTO> userConverter;


  /**
   * method for register user with social login
   *
   * @param service service name (google and others )
   * @param codeDTO dto contains the code to get the access token
   *
   * @return response entity with AuthDTO ( user info + token pair )
   *
   */
  @SneakyThrows
  @PostMapping("/auth-via/{service}")
  public ResponseEntity<AuthResponseDTO> auth(
      @PathVariable String service, @RequestBody CodeDTO codeDTO) {
    log.info("get new code dto " + codeDTO);
    final User user =
        oAuthService.processOAuthUser(OAuthType.valueOf(service.toUpperCase()), codeDTO.getCode());
    final TokenDTO token = tokenService.generateToken(user);
    final ResponseUserDTO userDTO = userConverter.toDTO(user);
    return ResponseEntity.ok().body(new AuthResponseDTO(token, userDTO));
  }
}
